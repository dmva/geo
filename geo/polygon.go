package geo

import (
	"bufio"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	geo "github.com/kellydunn/golang-geo"
)

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type PolygonChecker interface {
	Contains(point Point) bool // проверить, находится ли точка внутри полигона
	Allowed() bool             // разрешено ли входить в полигон
	RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

type Polygon struct {
	polygon *geo.Polygon
	allowed bool
}

func NewPolygon(points []Point, allowed bool) *Polygon {
	// используем библиотеку golang-geo для создания полигона
	geoPoints := make([]*geo.Point, len(points))
	for i, p := range points {
		geoPoints[i] = geo.NewPoint(p.Lat, p.Lng)
	}
	return &Polygon{
		polygon: geo.NewPolygon(geoPoints),
		allowed: allowed,
	}
}

func (p *Polygon) Contains(point Point) bool {
	return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p *Polygon) Allowed() bool {
	return p.allowed
}

func (p *Polygon) RandomPoint() Point {
	minLat, maxLat, minLng, maxLng := p.MinLat(), p.MaxLat(), p.MinLng(), p.MaxLng()
	for {
		lat := minLat + rand.Float64()*(maxLat-minLat)
		lng := minLng + rand.Float64()*(maxLng-minLng)
		if p.PointInPolygon(geo.NewPoint(lat, lng)) {
			return Point{
				lat, lng,
			}
		}
	}
}

func (p *Polygon) PointInPolygon(point *geo.Point) bool {
	var inside bool
	var j = len(p.polygon.Points()) - 1
	for i := 0; i < len(p.polygon.Points()); i++ {
		if (p.polygon.Points()[i].Lng() > point.Lng()) != (p.polygon.Points()[j].Lng() > point.Lng()) &&
			point.Lat() < (p.polygon.Points()[j].Lat()-p.polygon.Points()[i].Lat())*(point.Lng()-p.polygon.Points()[i].Lng())/
				(p.polygon.Points()[j].Lng()-p.polygon.Points()[i].Lng())+p.polygon.Points()[i].Lat() {
			inside = !inside
		}
		j = i
	}
	return inside
}

func (p *Polygon) MinLat() float64 {
	var minLat float64
	for _, point := range p.polygon.Points() {
		if minLat < point.Lat() || minLat == 0 {
			minLat = point.Lat()
		}
	}
	return minLat
}

func (p *Polygon) MaxLat() float64 {
	var maxLat float64
	for _, point := range p.polygon.Points() {
		if maxLat > point.Lat() {
			maxLat = point.Lat()
		}
	}
	return maxLat
}

func (p *Polygon) MinLng() float64 {
	var minLng float64
	for _, point := range p.polygon.Points() {
		if point.Lng() < minLng || minLng == 0 {
			minLng = point.Lng()
		}
	}
	return minLng
}

func (p *Polygon) MaxLng() float64 {
	var maxLng float64
	for _, point := range p.polygon.Points() {
		if point.Lng() > maxLng {
			maxLng = point.Lng()
		}
	}
	return maxLng
}

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
	// проверить, находится ли точка в разрешенной зоне
	if !allowedZone.Contains(point) {
		return false
	}
	for _, zone := range disabledZones {
		if zone.Contains(point) {
			return false
		}
	}

	return true
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
	var point Point
	// получение случайной точки в разрешенной зоне
	for {
		point = allowedZone.RandomPoint()
		if CheckPointIsAllowed(point, allowedZone, disabledZones) {
			break
		}
	}
	return point
}

func NewDisAllowedZone1() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	points := readPolygon("var noOrdersPolygon1")
	return NewPolygon(points, false)
}

func NewDisAllowedZone2() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	points := readPolygon("var noOrdersPolygon2")
	return NewPolygon(points, false)
}

func NewAllowedZone() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js
	points := readPolygon("var mainPolygon")
	return NewPolygon(points, true)
}

func readPolygon(typeZone string) []Point {
	var filePath string
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	if len(wd) == 4 {
		filePath = filepath.Join(wd, "public/js/polygon.js")
	} else {
		filePath = "../../../public/js/polygon.js"
	}

	file, err := os.Open(filePath)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var mainPolygon []Point
	var noOrdersPolygon1 []Point
	var noOrdersPolygon2 []Point
	var currentPolygon *[]Point
	for scanner.Scan() {
		line := scanner.Text()
		if match := strings.HasPrefix(line, "var mainPolygon"); match {
			currentPolygon = &mainPolygon
		} else if match := strings.HasPrefix(line, "var noOrdersPolygon1"); match {
			currentPolygon = &noOrdersPolygon1
		} else if match := strings.HasPrefix(line, "var noOrdersPolygon2"); match {
			currentPolygon = &noOrdersPolygon2
		}
		if currentPolygon != nil {
			re := regexp.MustCompile(`\[\s*(\d+\.\d+),\s*(\d+\.\d+)\s*\]`)
			matches := re.FindAllStringSubmatch(line, -1)
			for _, m := range matches {
				lat, _ := strconv.ParseFloat(m[1], 64)
				lng, _ := strconv.ParseFloat(m[2], 64)
				*currentPolygon = append(*currentPolygon, Point{Lat: lat, Lng: lng})
			}
		}
	}
	switch typeZone {
	case "var mainPolygon":
		return mainPolygon
	case "var noOrdersPolygon1":
		return noOrdersPolygon1
	case "var noOrdersPolygon2":
		return noOrdersPolygon2
	default:
		return []Point{}
	}
}
