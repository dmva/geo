package service

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/dmva/geo/geo"
	"gitlab.com/dmva/geo/module/order/models"
	"gitlab.com/dmva/geo/module/order/storage"
)

/*
В данном коде мы объявляем интерфейс Orderer, который определяет методы, которые должен реализовать
сервис для работы с заказами. Этот интерфейс включает в себя методы GetByRadius, Save, GetCount, RemoveOldOrders и GenerateOrder.

Далее мы объявляем структуру OrderService, которая реализует интерфейс Orderer. В этой структуре мы объявляем поля storage, allowedZone и disabledZones.

	•	storage - это объект, который реализует интерфейс OrderStorager для работы с хранилищем заказов.
	•	allowedZone - это объект, который реализует интерфейс PolygonChecker для проверки, находится ли точка в разрешенной зоне.
	•	disabledZones - это список объектов, которые реализуют интерфейс PolygonChecker для проверки, находится ли точка в запрещенных зонах.

Функция NewOrderService создает новый объект OrderService и возвращает его как интерфейс Orderer. В этой функции
мы передаем объекты storage, allowedZone и disabledZones в качестве параметров для инициализации полей структуры OrderService.
*/

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 2 * time.Minute
)

type Orderer interface {
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // возвращает заказы через метод storage.GetByRadius
	Save(ctx context.Context, order models.Order) error                                             // сохраняет заказ через метод storage.Save с заданным временем жизни OrderMaxAge
	GetCount(ctx context.Context) (int, error)                                                      // возвращает количество заказов через метод storage.GetCount
	RemoveOldOrders(ctx context.Context) error                                                      // удаляет старые заказы через метод storage.RemoveOldOrders с заданным временем жизни OrderMaxAge
	GenerateOrder(ctx context.Context) error                                                        // генерирует заказ в случайной точке из разрешенной зоны, с уникальным id, ценой и ценой доставки
}

// OrderService реализация интерфейса Orderer
// в нем должны быть методы GetByRadius, Save, GetCount, RemoveOldOrders, GenerateOrder
// данный сервис отвечает за работу с заказами
type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

func (o *OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	return o.storage.GetByRadius(ctx, lng, lat, radius, unit)
}

func (o *OrderService) Save(ctx context.Context, order models.Order) error {
	return o.storage.Save(ctx, order, orderMaxAge)
}

func (o *OrderService) GetCount(ctx context.Context) (int, error) {
	return o.storage.GetCount(ctx)
}

func (o *OrderService) RemoveOldOrders(ctx context.Context) error {
	return o.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (o *OrderService) GenerateOrder(ctx context.Context) error {
	point := geo.GetRandomAllowedLocation(o.allowedZone, o.disabledZones)

	price := (maxOrderPrice-minOrderPrice)*rand.Float64() + minOrderPrice
	deliveryPrice := (maxDeliveryPrice-minDeliveryPrice)*rand.Float64() + minDeliveryPrice
	order := models.Order{
		Price:         price,
		DeliveryPrice: deliveryPrice,
		Lng:           point.Lng,
		Lat:           point.Lat,
		CreatedAt:     time.Now(),
	}
	return o.storage.Save(ctx, order, orderMaxAge)
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}
