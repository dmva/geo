package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/dmva/geo/run"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
	// инициализация приложения
	app := run.NewApp()
	// запуск приложения
	err = app.Run()
	// в случае ошибки выводим ее в лог и завершаем работу с кодом 2
	if err != nil {
		log.Printf("error: %s", err)
		os.Exit(2)
	}
}
